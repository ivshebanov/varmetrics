package com.varmetrics.dao.repository;

import com.varmetrics.dao.model.Eur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EurRepository extends JpaRepository<Eur, Long> {

}
