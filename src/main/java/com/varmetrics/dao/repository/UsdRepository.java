package com.varmetrics.dao.repository;

import com.varmetrics.dao.model.Usd;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsdRepository extends JpaRepository<Usd, Long> {

}
